/**
 * @file ezi-view-lvgl.c
 *
 */

/*********************
 *      INCLUDES
 *********************/
#include "../lvgl/lvgl.h"

/*********************
 *      DEFINES
 *********************/

/**********************
 *      TYPEDEFS
 **********************/

/**********************
 *  STATIC PROTOTYPES
 **********************/
static void t1_create(lv_obj_t *parent);
static void t2_create(lv_obj_t *parent);
static void t3_create(lv_obj_t *parent);
static void t4_create(lv_obj_t *parent);
static void slider_event_cb(lv_obj_t *slider, lv_event_t e);
static void ta_event_cb(lv_obj_t *ta, lv_event_t e);
static void kb_event_cb(lv_obj_t *ta, lv_event_t e);
static void bar_anim(lv_task_t *t);
static void arc_anim(lv_obj_t *arc, lv_anim_value_t value);
static void linemeter_anim(lv_obj_t *linemeter, lv_anim_value_t value);
static void gauge_anim(lv_obj_t *gauge, lv_anim_value_t value);
static void table_event_cb(lv_obj_t *table, lv_event_t e);
// setup the theme
static void color_chg_event_cb(lv_obj_t *sw, lv_event_t e);

/**********************
 *  STATIC VARIABLES
 **********************/
static lv_obj_t *tv;
static lv_obj_t *t1;
static lv_obj_t *t2;
static lv_obj_t *t3;
static lv_obj_t *t4;
static lv_obj_t *kb;

static lv_style_t style_box;

/**********************
 *      MACROS
 **********************/

/**********************
 *   GLOBAL FUNCTIONS
 **********************/

#include "stdio.h"
#include "ezi_view_lvgl.h"

void ezi_lvgl(void)
{
    printf("Running eZi LVGL\n");
}

void create_dsp1_display(void)
{
    printf("Running eZi dsp1 display\n");

    // Setup the TAB view
    tv = lv_tabview_create(lv_scr_act(), NULL);
    LV_THEME_DEFAULT_INIT(lv_theme_get_color_primary(), lv_theme_get_color_secondary(),
                          1,
                          lv_theme_get_font_small(), lv_theme_get_font_normal(), lv_theme_get_font_subtitle(), lv_theme_get_font_title());

    t1 = lv_tabview_add_tab(tv, "Info");
    t2 = lv_tabview_add_tab(tv, "Temperature");
    t3 = lv_tabview_add_tab(tv, "Humidity");
    t4 = lv_tabview_add_tab(tv, "Static Pressure");
  

    lv_style_init(&style_box);
    lv_style_set_value_align(&style_box, LV_STATE_DEFAULT, LV_ALIGN_OUT_TOP_LEFT);
    lv_style_set_value_ofs_y(&style_box, LV_STATE_DEFAULT, -LV_DPX(10));
    lv_style_set_margin_top(&style_box, LV_STATE_DEFAULT, LV_DPX(30));

    t1_create(t1);//Info
    t2_create(t2);//Temperature
    t3_create(t3);//Humidity
    t4_create(t4);//Pressure

}

/*
void keypad_change_cb(lv_obj_t * btnm, lv_event_t event)
{
    if (event == LV_EVENT_VALUE_CHANGED)
    {
        const char * txt = lv_textarea_get_text(btnm);
        printf("Keypad value event changed\n");
   }
}

void numeric_keypad(void)
{
    static lv_style_t style_keypad_value;
    lv_obj_t * btnm = lv_btnmatrix_create(t2,NULL);
    lv_btnmatrix_set_map(btnm,btn_map);
    lv_obj_set_height(btnm,250);
    lv_obj_align(btnm,t2, LV_ALIGN_IN_TOP_RIGHT,5,5);
    lv_obj_set_event_cb(btnm,keypad_change_cb);
    //Create Text display area
    lv_obj_t * keypad_value = lv_textarea_create(t2,NULL);
    lv_style_init(&style_keypad_value);
    lv_obj_set_size(keypad_value, 180, 40);
    lv_obj_align(keypad_value,btnm,LV_ALIGN_OUT_LEFT_TOP,-5,0);
    lv_style_set_text_font(&style_keypad_value, LV_STATE_DEFAULT, &lv_font_montserrat_18);
    lv_style_set_text_color(&style_keypad_value, LV_STATE_DEFAULT, LV_COLOR_MAKE(0x00,0x00,0x00));
    lv_style_set_bg_color(&style_keypad_value, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff,0xff,0xff));
    lv_obj_add_style(keypad_value,LV_STATE_DEFAULT,&style_keypad_value);
    lv_textarea_set_text_align(keypad_value,LV_LABEL_ALIGN_CENTER);   
    lv_textarea_set_text(keypad_value,"123456");
}
*/




static lv_obj_t * kb;
static lv_obj_t * ta;


static void kb_event_cb(lv_obj_t * keyboard, lv_event_t e)
{
    lv_keyboard_def_event_cb(kb, e);
    if(e == LV_EVENT_CANCEL) {
        lv_keyboard_set_textarea(kb, NULL);
        lv_obj_del(kb);
        kb = NULL;
        lv_obj_del(ta);
        ta= NULL;
    }
}

static void kb_create(void)
{
    switch(lv_tabview_get_tab_act(tv))
    {
        case 0:
            break;
        case 1:
            kb = lv_keyboard_create(t2, NULL);
            break;
        case 2:
            kb = lv_keyboard_create(t3, NULL);
            break;
        case 3:
            kb = lv_keyboard_create(t4, NULL);
            break;
        default:
            break;
    }
  
    lv_keyboard_set_mode(kb,LV_KEYBOARD_MODE_NUM);
    lv_obj_set_height(kb,250);
    lv_obj_set_width(kb,240);
    lv_obj_align(kb,t2, LV_ALIGN_IN_TOP_RIGHT,5,5);
    lv_keyboard_set_cursor_manage(kb, true);
    lv_obj_set_event_cb(kb, kb_event_cb);
    lv_keyboard_set_textarea(kb, ta);

}

void lv_ex_keyboard_1(void)
{
    static lv_style_t style_ta;
    /*Create a text area. The keyboard will write here*/
    ta  = lv_textarea_create(lv_scr_act(), NULL);

    lv_style_init(&style_ta);
    lv_obj_set_size(ta, 180, 40);
    lv_obj_set_pos(ta,40,60);
    lv_style_set_text_font(&style_ta, LV_STATE_DEFAULT, &lv_font_montserrat_18);
    lv_style_set_text_color(&style_ta, LV_STATE_DEFAULT, LV_COLOR_MAKE(0x00,0x00,0x00));
    lv_style_set_bg_color(&style_ta, LV_STATE_DEFAULT, LV_COLOR_MAKE(0xff,0xff,0xff));
    lv_obj_add_style(ta,LV_STATE_DEFAULT,&style_ta);
    lv_textarea_set_text_align(ta,LV_LABEL_ALIGN_CENTER); 
    lv_textarea_set_text(ta, "");
    kb_create();
    
}

void gauge_change_cb(lv_obj_t * gauge, lv_event_t event)
{
    if (event == LV_EVENT_LONG_PRESSED)
    {
        printf("Temp Gauge Long Pressed\n");
        printf("Current tab %u\n",lv_tabview_get_tab_act(tv));
        lv_ex_keyboard_1();
    }
}
static void t1_create(lv_obj_t *parent)
{
    static lv_style_t style_info_text;
    lv_obj_t * info1 = lv_label_create(parent,NULL);
    lv_obj_t * info2 = lv_label_create(parent,NULL);
    lv_obj_t * info3 = lv_label_create(parent,NULL);
    lv_obj_t * info4 = lv_label_create(parent,NULL);
    lv_style_init(&style_info_text);

    lv_style_set_text_font(&style_info_text, LV_STATE_DEFAULT, &lv_font_montserrat_18);
    //Write the controller part number
    lv_obj_add_style(info1, LV_LABEL_PART_MAIN, &style_info_text);
    lv_label_set_align(info1,LV_LABEL_ALIGN_LEFT);
    lv_obj_set_pos(info1, 10, 5);
    lv_label_set_recolor(info1, true);
    lv_label_set_text(info1,"#D8D8D8 eZi-DSP-1 Revision 1.0");

    // Write the Plant dsescriptor
    lv_obj_add_style(info2, LV_LABEL_PART_MAIN, &style_info_text);
    lv_label_set_align(info2,LV_LABEL_ALIGN_LEFT);
    lv_obj_set_pos(info2, 10, 55);
    lv_label_set_recolor(info2, true);
    lv_label_set_text(info2,"#000000 AIR HANDLING UNIT 1 EAST");

    // Write the Modbus Text
    lv_obj_add_style(info3, LV_LABEL_PART_MAIN, &style_info_text);
    lv_label_set_align(info3,LV_LABEL_ALIGN_LEFT);
    lv_obj_set_pos(info3, 10, 105);
    lv_label_set_recolor(info3, true);
    lv_label_set_text(info3,"#000000 Modbus");

    // Write the Modbus baud
    lv_obj_add_style(info4, LV_LABEL_PART_MAIN, &style_info_text);
    lv_label_set_align(info4,LV_LABEL_ALIGN_LEFT);
    lv_obj_set_pos(info4, 100, 105);
    lv_label_set_recolor(info4, true);
    lv_label_set_text(info4,"#000000 9600,8,n,1");
}

static void t2_create(lv_obj_t *parent)
{

    // draw gauge
    static lv_color_t needle1_colors[2];
    static lv_style_t style_bar_text;

    needle1_colors[0] = LV_COLOR_GREEN;  // Temperature
    needle1_colors[1] = LV_COLOR_ORANGE; // Setpoint

    lv_obj_t *gauge = lv_gauge_create(parent, NULL);
    lv_gauge_set_range(gauge, 0, 30);
    lv_gauge_set_critical_value(gauge, 35);
    lv_gauge_set_needle_count(gauge, 2, needle1_colors);
    lv_gauge_set_value(gauge, 0, 22); // Temperature
    lv_gauge_set_value(gauge, 1, 20); // Setpoint
    lv_obj_set_style_local_border_color(gauge,LV_GAUGE_PART_MAIN,LV_STATE_FOCUSED,LV_COLOR_WHITE);
    lv_obj_set_style_local_border_color(gauge,LV_GAUGE_PART_MAIN,LV_STATE_DEFAULT,LV_COLOR_WHITE);
    lv_obj_set_size(gauge, 235, 235);
    lv_obj_set_pos(gauge, 10, 5);
    lv_obj_set_event_cb(gauge, gauge_change_cb);

    //Create Bar1
    lv_obj_t *bar_1 = lv_bar_create(parent, NULL);
    lv_bar_set_type(bar_1,LV_BAR_TYPE_SYMMETRICAL);
    lv_obj_set_height(bar_1, 200);
    lv_obj_set_width(bar_1, 10);
    lv_bar_set_range(bar_1, 0, 100);
    lv_obj_set_pos(bar_1, 315, 15);
    //Background
    lv_obj_set_style_local_bg_color(bar_1,LV_BAR_PART_BG,LV_STATE_DEFAULT,LV_COLOR_MAKE(0x31,0x33,0x32));
    lv_obj_set_style_local_bg_color(bar_1,LV_BAR_PART_INDIC,LV_STATE_DEFAULT,LV_COLOR_RED);
    //Now set the value
    lv_bar_set_value(bar_1, 50, 0);

    //Create Bar2
    lv_obj_t *bar_2 = lv_bar_create(parent, NULL);
    lv_bar_set_type(bar_2,LV_BAR_TYPE_SYMMETRICAL);
    lv_obj_set_height(bar_2, 200);
    lv_obj_set_width(bar_2, 10);
    lv_bar_set_range(bar_1, 0, 100);
    lv_obj_set_pos(bar_2, 425, 15);
    //Background
    lv_obj_set_style_local_bg_color(bar_2,LV_BAR_PART_BG,LV_STATE_DEFAULT,LV_COLOR_MAKE(0x31,0x33,0x32));
    lv_obj_set_style_local_bg_color(bar_2,LV_BAR_PART_INDIC,LV_STATE_DEFAULT,LV_COLOR_BLUE);
    //Now set the value
    lv_bar_set_value(bar_2, 30, 0);

    // Bar1 text value
    lv_obj_t *bar1value = lv_label_create(parent, NULL);
    lv_style_init(&style_bar_text);
    lv_style_set_text_font(&style_bar_text, LV_STATE_DEFAULT, &lv_font_montserrat_18);
    lv_obj_add_style(bar1value, LV_LABEL_PART_MAIN, &style_bar_text);
    lv_obj_align(bar1value, bar_1, LV_ALIGN_OUT_BOTTOM_LEFT, -25, 15);
    lv_label_set_recolor(bar1value, true);
    lv_label_set_text(bar1value, "#ffffff 10 %");

    // Bar2 text value
    lv_obj_t *bar2value = lv_label_create(parent, NULL);
    lv_obj_add_style(bar2value, LV_LABEL_PART_MAIN, &style_bar_text);
    lv_obj_align(bar2value, bar_2, LV_ALIGN_OUT_BOTTOM_LEFT, -25, 15);
    lv_label_set_recolor(bar2value, true);
    lv_label_set_text(bar2value, "#ffffff 80 %");


    // Temperature
    lv_obj_t *gauge_mv_text = lv_label_create(parent, NULL);
    lv_label_set_align(gauge_mv_text, LV_LABEL_ALIGN_LEFT);
    lv_obj_align(gauge_mv_text, gauge, LV_ALIGN_IN_BOTTOM_LEFT, 0, 14);
    lv_label_set_recolor(gauge_mv_text, true);
    lv_label_set_text(gauge_mv_text, "#008000 Temperature");
    // Setpoint
    lv_obj_t *gauge_sp_text = lv_label_create(parent, NULL);
    lv_label_set_align(gauge_sp_text, LV_LABEL_ALIGN_RIGHT);
    lv_obj_align(gauge_sp_text, gauge, LV_ALIGN_IN_BOTTOM_RIGHT, -50, 14);
    lv_label_set_recolor(gauge_sp_text, true);
    lv_label_set_text(gauge_sp_text, "#ffa500 Setpoint");
}

static void t3_create(lv_obj_t *parent)
{

    // draw gauge
    static lv_color_t needle1_colors[2];
    static lv_style_t style_bar_text;

    needle1_colors[0] = LV_COLOR_GREEN;  // Humidity
    needle1_colors[1] = LV_COLOR_ORANGE; // Setpoint

    lv_obj_t *gauge = lv_gauge_create(parent, NULL);
    lv_gauge_set_range(gauge, 0, 100);
    lv_gauge_set_critical_value(gauge, 101);
    lv_gauge_set_needle_count(gauge, 2, needle1_colors);
    lv_gauge_set_value(gauge, 0, 22); // Humidity
    lv_gauge_set_value(gauge, 1, 20); // Setpoint
    lv_obj_set_size(gauge, 235, 235);
    lv_obj_set_pos(gauge, 10, 5);
    lv_obj_set_style_local_border_color(gauge,LV_GAUGE_PART_MAIN,LV_STATE_FOCUSED,LV_COLOR_WHITE);
    lv_obj_set_style_local_border_color(gauge,LV_GAUGE_PART_MAIN,LV_STATE_DEFAULT,LV_COLOR_WHITE);
    lv_obj_set_event_cb(gauge, gauge_change_cb);


    //Create Bar1
    lv_obj_t *bar_1 = lv_bar_create(parent, NULL);
    lv_bar_set_type(bar_1,LV_BAR_TYPE_SYMMETRICAL);
    lv_obj_set_height(bar_1, 200);
    lv_obj_set_width(bar_1, 10);
    lv_bar_set_range(bar_1, 0, 100);
    lv_obj_set_pos(bar_1, 370, 15);
    //Background
    lv_obj_set_style_local_bg_color(bar_1,LV_BAR_PART_BG,LV_STATE_DEFAULT,LV_COLOR_MAKE(0x31,0x33,0x32));
    lv_obj_set_style_local_bg_color(bar_1,LV_BAR_PART_INDIC,LV_STATE_DEFAULT,LV_COLOR_MAKE(0x29,0x9a,0x72));
    //Now set the value
    lv_bar_set_value(bar_1, 50, 0);

    // Bar1 text value
    lv_obj_t *bar1value = lv_label_create(parent, NULL);
    lv_style_init(&style_bar_text);
    lv_style_set_text_font(&style_bar_text, LV_STATE_DEFAULT, &lv_font_montserrat_18);
    lv_obj_add_style(bar1value, LV_LABEL_PART_MAIN, &style_bar_text);
    lv_obj_align(bar1value, bar_1, LV_ALIGN_OUT_BOTTOM_LEFT, -25, 15);
    lv_label_set_recolor(bar1value, true);
    lv_label_set_text(bar1value, "#ffffff 50 %");

    // Humidity
    lv_obj_t *gauge_mv_text = lv_label_create(parent, NULL);
    lv_label_set_align(gauge_mv_text, LV_LABEL_ALIGN_LEFT);
    lv_obj_align(gauge_mv_text, gauge, LV_ALIGN_IN_BOTTOM_LEFT, 0, 14);
    lv_label_set_recolor(gauge_mv_text, true);
    lv_label_set_text(gauge_mv_text, "#008000 Humidity");
    // Setpoint
    lv_obj_t *gauge_sp_text = lv_label_create(parent, NULL);
    lv_label_set_align(gauge_sp_text, LV_LABEL_ALIGN_RIGHT);
    lv_obj_align(gauge_sp_text, gauge, LV_ALIGN_IN_BOTTOM_RIGHT, -50, 14);
    lv_label_set_recolor(gauge_sp_text, true);
    lv_label_set_text(gauge_sp_text, "#ffa500 Setpoint");
}

static void t4_create(lv_obj_t *parent)
{

    // draw gauge
    static lv_color_t needle1_colors[2];
    static lv_style_t style_bar_text;

    needle1_colors[0] = LV_COLOR_GREEN;  // Pressure
    needle1_colors[1] = LV_COLOR_ORANGE; // Setpoint

    lv_obj_t *gauge = lv_gauge_create(parent, NULL);
    lv_gauge_set_range(gauge, 0, 500);
    lv_gauge_set_critical_value(gauge, 501);
    lv_gauge_set_needle_count(gauge, 2, needle1_colors);
    lv_gauge_set_value(gauge, 0, 220); // Pressure
    lv_gauge_set_value(gauge, 1, 200); // Setpoint
    lv_obj_set_size(gauge, 235, 235);
    lv_obj_set_pos(gauge, 10, 5);
    lv_obj_set_style_local_border_color(gauge,LV_GAUGE_PART_MAIN,LV_STATE_FOCUSED,LV_COLOR_WHITE);
    lv_obj_set_style_local_border_color(gauge,LV_GAUGE_PART_MAIN,LV_STATE_DEFAULT,LV_COLOR_WHITE);
    lv_obj_set_event_cb(gauge, gauge_change_cb);


    //Create Bar1
    lv_obj_t *bar_1 = lv_bar_create(parent, NULL);
    lv_bar_set_type(bar_1,LV_BAR_TYPE_SYMMETRICAL);
    lv_obj_set_height(bar_1, 200);
    lv_obj_set_width(bar_1, 10);
    lv_bar_set_range(bar_1, 0, 100);
    lv_obj_set_pos(bar_1, 370, 15);
    //Background
    lv_obj_set_style_local_bg_color(bar_1,LV_BAR_PART_BG,LV_STATE_DEFAULT,LV_COLOR_MAKE(0x31,0x33,0x32));
    lv_obj_set_style_local_bg_color(bar_1,LV_BAR_PART_INDIC,LV_STATE_DEFAULT,LV_COLOR_MAKE(0x69,0x29,0x9a));
    //Now set the value
    lv_bar_set_value(bar_1, 30, 0);

    // Bar1 text value
    lv_obj_t *bar1value = lv_label_create(parent, NULL);
    lv_style_init(&style_bar_text);
    lv_style_set_text_font(&style_bar_text, LV_STATE_DEFAULT, &lv_font_montserrat_18);
    lv_obj_add_style(bar1value, LV_LABEL_PART_MAIN, &style_bar_text);
    lv_obj_align(bar1value, bar_1, LV_ALIGN_OUT_BOTTOM_LEFT, -25, 15);
    lv_label_set_recolor(bar1value, true);
    lv_label_set_text(bar1value, "#ffffff 50 %");

    // Pressure
    lv_obj_t *gauge_mv_text = lv_label_create(parent, NULL);
    lv_label_set_align(gauge_mv_text, LV_LABEL_ALIGN_LEFT);
    lv_obj_align(gauge_mv_text, gauge, LV_ALIGN_IN_BOTTOM_LEFT, 0, 14);
    lv_label_set_recolor(gauge_mv_text, true);
    lv_label_set_text(gauge_mv_text, "#008000 Pressure");
    // Setpoint
    lv_obj_t *gauge_sp_text = lv_label_create(parent, NULL);
    lv_label_set_align(gauge_sp_text, LV_LABEL_ALIGN_RIGHT);
    lv_obj_align(gauge_sp_text, gauge, LV_ALIGN_IN_BOTTOM_RIGHT, -50, 14);
    lv_label_set_recolor(gauge_sp_text, true);
    lv_label_set_text(gauge_sp_text, "#ffa500 Setpoint");
}



